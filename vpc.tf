provider "aws" {
  region     = "ap-south-1"
  profile    = "manish"
}

// create key pair
resource "aws_key_pair" "terrformkey" {
  key_name   = "terrform-key"
  public_key = file("E:/hybrid-multi-cloud/terraform/aws/mypublic.key")
 }
// create vpc
resource "aws_vpc" "manishvpc-tf" {
  cidr_block       = "10.0.0.0/16"
  instance_tenancy = "default"
  enable_dns_hostnames = true
  tags = {
    Name = "tfvpc"
  }
}

// create public subnet
resource "aws_subnet" "web-subnet-tf" {
  vpc_id     = aws_vpc.manishvpc-tf.id
  cidr_block = "10.0.1.0/24"
  availability_zone = "ap-south-1a"
  map_public_ip_on_launch = true
  tags = {
    Name = "websubnettf"
  }
}

// create private subnet
resource "aws_subnet" "database-subnet-tf" {
  vpc_id     = aws_vpc.manishvpc-tf.id
  cidr_block = "10.0.2.0/24"
  availability_zone = "ap-south-1b"
  tags = {
    Name = "dbsubnettf"
  }
}

// create internet gateway for public subnet
resource "aws_internet_gateway" "tf-igw" {
  vpc_id = aws_vpc.manishvpc-tf.id

  tags = {
    Name = "tfigw"
  }
}

// create route table for public subnet for i-gateway
resource "aws_route_table" "route-public-subnet-i-gateway" {
  vpc_id = aws_vpc.manishvpc-tf.id
  route {
    //cidr_block = "10.0.0.0/16"
	cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.tf-igw.id
  } 
  tags = {
    Name = "route-public-subnet"
  }
 } 
 
// assosiate to public subnet
resource "aws_route_table_association" "assosiate-route-public-subnet" {
  subnet_id      = aws_subnet.web-subnet-tf.id
  route_table_id = aws_route_table.route-public-subnet-i-gateway.id
}

// create sg for wordpress
resource "aws_security_group" "allow_http_ssh" {
  name        = "allow_http-ssh"
  description = "Allow http and ssh inbound traffic"
  vpc_id      = aws_vpc.manishvpc-tf.id

egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags = {
    Name = "allow_http-ssh"
  }
}

// add sg rule for PING
resource "aws_security_group_rule" "ping_rule" {
  type              = "ingress"
  from_port         = -1
  to_port           = -1
  protocol          = "ICMP"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = aws_security_group.allow_http_ssh.id
}

// add sg rule for port no 80 and 22
resource "aws_security_group_rule" "http_rule" {
  type              = "ingress"
  from_port         = 22
  to_port           = 22
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = aws_security_group.allow_http_ssh.id
}

resource "aws_security_group_rule" "ssh_rule" {
  type              = "ingress"
  from_port         = 80
  to_port           = 80
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = aws_security_group.allow_http_ssh.id
}

// create ec2 in public subnet with wordpress
resource "aws_instance" "web" {
depends_on = [
    aws_security_group.allow_http_ssh,
  ]
  ami = "ami-0979674e4a8c6ea0c"   
  instance_type = "t2.micro"
 // security_groups = ["allow_http-ssh"]
  vpc_security_group_ids = [aws_security_group.allow_http_ssh.id]
  key_name        =  aws_key_pair.terrformkey.key_name
  subnet_id = aws_subnet.web-subnet-tf.id
  
  tags = {
    Name = "wordpress_os"
  }
}

// create sg for mysql
resource "aws_security_group" "mysql-sg" {
  name        = "mysql-sg"
  description = "Allow mysql inbound traffic"
  vpc_id      = aws_vpc.manishvpc-tf.id

  tags = {
    Name = "sg-private-subnet"
  }
}
resource "aws_security_group_rule" "mysql" {
  type              = "ingress"
  from_port         = 3306
  to_port           = 3306
  protocol          = "tcp"
  //cidr_blocks       = ["0.0.0.0/0"]
  source_security_group_id = aws_security_group.allow_http_ssh.id
  security_group_id = aws_security_group.mysql-sg.id
}

// create ec2 in private subnet with mysql
resource "aws_instance" "mysql" {
depends_on = [
    aws_security_group.mysql-sg,
	aws_security_group_rule.mysql,
  ]
  //ami           = "ami-08706cb5f68222d09"
  ami = "ami-76166b19"
  instance_type = "t2.micro"
  //security_groups = ["sg-private-subnet"]
  vpc_security_group_ids = [aws_security_group.mysql-sg.id]
  key_name        =  aws_key_pair.terrformkey.key_name
  subnet_id = aws_subnet.database-subnet-tf.id
  
  tags = {
    Name = "mysql_os"
  }
}

// create sg for bastion host
resource "aws_security_group" "allowssh_bastion" {
  name        = "allow_ssh-bastion"
  description = "Allow http and ssh inbound traffic"
  vpc_id      = aws_vpc.manishvpc-tf.id
egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags = {
    Name = "allowssh_bastion"
  }
}

resource "aws_security_group_rule" "bastionssh_rule" {
  type              = "ingress"
  from_port         = 22
  to_port           = 22
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = aws_security_group.allowssh_bastion.id
}

// create ec2 in public subnet with bastion host
resource "aws_instance" "bastion" {
depends_on = [
    aws_security_group.allowssh_bastion,
  ]
  ami = "ami-0447a12f28fddb066"   
  instance_type = "t2.micro"
 // security_groups = ["allow_http-ssh"]
  vpc_security_group_ids = [aws_security_group.allowssh_bastion.id]
  key_name        =  aws_key_pair.terrformkey.key_name
  subnet_id = aws_subnet.web-subnet-tf.id
  
  tags = {
    Name = "bastion_os"
  }
}

// create sg for mysql host
resource "aws_security_group" "allowssh_mysqlserver" {
  name        = "allow_ssh-mysql"
  description = "Allow ssh inbound traffic"
  vpc_id      = aws_vpc.manishvpc-tf.id
egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags = {
    Name = "allowssh_bastion"
  }
}

resource "aws_security_group_rule" "mysqlserver_rule" {
  type              = "ingress"
  from_port         = 22
  to_port           = 22
  protocol          = "tcp"
  //cidr_blocks       = ["0.0.0.0/0"]
  source_security_group_id = aws_security_group.allow_http_ssh.id
  security_group_id = aws_security_group.allowssh_bastion.id
}
// o/p s 
output "my_vpcid" {
   value = aws_vpc.manishvpc-tf.id
}
output "my_rid" {
   value = aws_route_table.route-public-subnet-i-gateway.id
}

output "my_wordpressip" {
   value = aws_instance.web.public_ip
}

resource "aws_eip" "nat" {
  //instance = "${aws_instance.web.id}"
  vpc  = true
}

// create nat gateway to access innternet in public-subnet
resource "aws_nat_gateway" "gw" {
depends_on = [
    aws_eip.nat,
  ]
  allocation_id = aws_eip.nat.id
  subnet_id     = aws_subnet.web-subnet-tf.id

  tags = {
    Name = "NAT"
  }
}


// create route table for public subnet for nat gateway
resource "aws_route_table" "route-public-subnet-nat-g" {
depends_on = [
    aws_nat_gateway.gw,
  ]
  vpc_id = aws_vpc.manishvpc-tf.id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_nat_gateway.gw.id
  } 
  tags = {
    Name = "route-public-subnet"
  }
 }
 // assosiate to public subnet
resource "aws_route_table_association" "assosiate-route-private-subnet" {
  subnet_id      = aws_subnet.database-subnet-tf.id
  route_table_id = aws_route_table.route-public-subnet-nat-g.id
}

